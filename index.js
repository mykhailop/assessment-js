/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

exports.stripPrivateProperties = (keysToOmit = [], list = []) => {
    if (!keysToOmit || !keysToOmit.length || !Array.isArray(list)) {
        return list;
    }

    return list.map((entry) => {
        return Object.keys(entry).reduce((memo, key) => {
            if (keysToOmit.includes(key)) {
                return memo;
            }

            return { ...memo, [key]: entry[key] };
        }, {});
    });
};

exports.excludeByProperty = (keyToOmit = '', list = []) => {
    if (!keyToOmit || !Array.isArray(list)) {
        return list;
    }

    // we could also use here Set to reduce amount of loops
    return list.filter((entry) => !Object.keys(entry).includes(keyToOmit));
};


const getSum = (list) => {
    const valKey = 'val';

    return list.reduce((memo, value) => {
        if (value && Number.isInteger(value[valKey])) {
            memo = memo + value[valKey];
        }

        return memo;
    }, 0);
};

exports.sumDeep = (list) => {
    const objKey = 'objects';

    if (!Array.isArray(list)) {
        return list;
    }

    return list.map((entry) => ({
        [objKey]: getSum(entry[objKey]),
    }));
};

exports.applyStatusColor = (colors, statuses) => {
    if (!Array.isArray(statuses) || !colors) {
        return statuses;
    }

    const colorsList = Object.keys(colors);

    return statuses.reduce((memo, entry) => {
        // we could create colors map previously to reduce amount of loops, e.g.:
        // { 400: 'red', 404: 'red', 200: 'green' } and just pass proper value below like:
        // const color = colorsMap[entry.status]; 
        const color = colorsList.find((key) => colors[key].includes(entry.status));

        if (!color) {
            return memo;
        }

        return memo.concat({ ...entry, color });
    }, []);
};

exports.createGreeting = (greet, text) => (name) => greet(text, name);

exports.setDefaults = (defaultValue) => (value = {}) => ({ ...defaultValue, ...value });

exports.fetchUserByNameAndUsersCompany = async (username, { fetchStatus, fetchUsers, fetchCompanyById }) => {
    let user, status;

    await Promise.all([fetchStatus(), fetchUsers()]).then(([_status, users]) => {
        status = _status;

        user = users.find((entry) => entry.name === username);
    });

    const company = await fetchCompanyById(user.companyId);

    return { status, company, user };
};
